#language: pt

@all @login
Funcionalidade: Realizar Login

  Esquema do Cenario: Realizar Login com <condition>
    Dado que o usuário tente logar com o usuário "<user>"
    E o usuário digite a senha "<password>"
    Quando pressionar o botão "Login"
    Então o usuário deve ser logado com "<message>"
    Exemplos:
      | condition                        | user        | password  | message |
      | usuário gestor                   | gestor      | padrão    | sucesso |
      | usuário gestor-scob              | gestor-scob | padrão    | sucesso |
      | usuário válido e senha incorreta | gestor      | incorreta | falha   |
      | usuário inválido e senha correta | inválido    | padrão    | falha   |

  @bug @todo
#        | usuário pontal                   | pontal      | padrão    | sucesso |
# Nenhuma mensagem de erro é exibida ao usuário, e o botão continua Habilitado.
#      | usuário vazio e senha correta | ""     | padrão   | falha   |
#      | usuário gestor e senha vazia  | gestor | ""       | falha   |

