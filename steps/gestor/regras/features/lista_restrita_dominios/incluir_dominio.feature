#language: pt

@all @gestor @regras @lista_restrita_dominios @incluir_nova_restricao
Funcionalidade: Incluir uma nova restrição "Lista Restrita Domínios"

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Acessar a página inicial "Lista Restrita Domínios"
    E esteja na página incial da opção "Lista Restrita Domínios"
    Quando clicar no botão "Nova Restrição" da janela inicial "Lista Restrita Domínios"
    Então a página de "Nova Restrição" das regras deverá ser aberta

  Esquema do Cenário: Criar uma "Nova Restrição" com o dominio "<domain_extension>"
    E esteja na página incial "Nova Restrição"
    E preencha o campo "Extensão Dominio" com "<domain_extension>"
    Quando clicar no botão "Salvar" na janela de "Nova Restrição"
    Então o dominio "<domain_extension>" deverá ser criado com sucesso na janela "Lista Restrita Domínios"
    Exemplos:
      | domain_extension |
      | teste.com.br     |

  Cenário: Criar mais de uma restrição por vez
    E esteja na página incial "Nova Restrição"
    E preencha o campo "Extensão Dominio" com "mais de um dominio"
    Quando clicar no botão "Salvar" na janela de "Nova Restrição"
    Então os dominios criados deverão ser criados com sucesso na janela "Lista Restrita Domínios"

