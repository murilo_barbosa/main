#language: pt

@all @gestor @regras @visualizar_regras_ddd
Funcionalidade: Visualizar regras de DDD

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: No portal do Gestor, acessar a página de "DDD"
    Quando clicar no botão "Configurar" da opção "DDD"
    Então a janela inicial "DDD" das regras deverá ser aberta

  Esquema do Cenário: Na página de "Regras", <status> a regra DDD
    Quando clicar no botão "<status>" da opção "DDD"
    Então a regra deverá estar "<result>"
    Exemplos:
      | status      | result       |
      | Desabilitar | Desabilitada |
      | Habilitar   | Habilitada   |

  Esquema do Cenário: Na página do DDD, alterar a ordenação de "<condition_1>" para "<condition_2>"
    Quando clicar no botão "Configurar" da opção "DDD"
    E alterar a ordenação de "<condition_1>" para "<condition_2>" na janela de "DDD"
    Então a ordenação por "<condition_2>" deverá ser exibida de forma "crescente" na janela de "DDD"
    Exemplos:
      | condition_1 | condition_2 |
      | Estado      | DDD         |
      | DDD         | Estado      |

  Esquema do Cenário: Na página do DDD, alterar a ordenação de "<condition_1>" para "<condition_2>" e ser exibida de forma "Decrescente"
    Quando clicar no botão "Configurar" da opção "DDD"
    E alterar a ordenação de "<condition_1>" para "<condition_2>" na janela de "DDD"
    E clicar no botão para alterar a ordem de "crescente" para "decrescente" na página de DDD
    Então a ordenação por "<condition_2>" deverá ser exibida de forma "decrescente" na janela de "DDD"
    Exemplos:
      | condition_1 | condition_2 |
      | Estado      | DDD         |
      | DDD         | Estado      |

  @todo # Verificar um meio eficiente de validar o teste. (Talvez fazedno um document.query direto pelo JS)
  Esquema do Cenário: Realizar buscas por "<condition>"
    Quando clicar no botão "Configurar" da opção "DDD"
    E realizar uma busca por um "<condition>"
    Então todas as regras com o "<condition>" selecionado deverá ser exibida
    Exemplos:
      | condition |
      | Estado    |
      | DDD       |