#language: pt

@all @gestor @regras @ddd @excluir_ddd
Funcionalidade: Regras Editar DDD

  Cenário: Excluir uma regra de DDD
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"
    E tenha uma regra de DDD previamente cadastrada
    Quando clicar no botão excluir da página de "DDD"
    E confirmar clicando no botão "Remover regra"
    Então a regra de "DDD" deverá ser excluida com sucesso