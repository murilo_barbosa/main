#language: pt

@all @gestor @regras @limitacao_disparos @geral_limitacao_disparos
Funcionalidade: Limitação Disparos

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Acessar a página inicial "Limitação Disparos"
    Quando clicar no botão "Configurar" da opção "Limitação Disparos"
    Então a janela inicial "Limitação Disparos" das regras deverá ser aberta

  Esquema do Cenário: Na página de "Regras", <status> a regra "Limitação Disparos"
    Quando clicar no botão "<status>" da opção "Limitação Disparos"
    Então a regra da opção "Limitação Disparos" deverá estar "<result>"
    Exemplos:
      | status      | result       |
      | Desabilitar | Desabilitada |
      | Habilitar   | Habilitada   |

  Esquema do Cenário: Na página "Lista Restrita Domínios", alterar a ordenação de "<condition_1>" para "<condition_2>"
    E esteja na janela de "Limitação Disparos"
    Quando alterar a ordenação de "<condition_1>" para "<condition_2>" na janela de "Limitação Disparos"
    Então a ordenação por "<condition_2>" deverá ser exibida de forma "crescente" na janela de "Limitação Disparos"
    Exemplos:
      | condition_1 | condition_2 |
      | Nome        | Carteira    |
      | Carteira    | Nome        |

  Cenário: Na página "Limitação Disparos", reordenar a lista de forma "Decrescente"
    E esteja na janela de "Limitação Disparos"
    Quando clicar no botão para reordenar a ordem de exibição na janela de "Limitação Disparos"
    Então a ordenação deverá ser exibida de forma "decrescente" na janela de "Limitação Disparos"

  @todo
#  Esquema do Cenário: Desmarcar o checkbox <checkbox_name> do dropdown "Carteira" na janela "Limitação de disparos"
#    E esteja na janela de "Limitação Disparos"
#    E clicar no dropdown "Carteira" da janela "Limitação Disparos"
#    Quando selecionar o "<checkbox_name>" do dropdown "Carteira"
#    Exemplos:
#      | checkbox_name    |
#      | Selecionar todas |
#      | WO               |

  @todo
  Esquema do Cenário: Realizar buscas por "<condition>"
    E esteja na janela de "Limitação Disparos"
    Quando realizar uma busca por "<condition>" na janela de "Limitação Disparos"
    Então todas as regras com o/a "<condition>" selecionado deverá ser exibida na janela de "Limitação Disparos"
    Exemplos:
      | condition |
      | Padrão    |
      | WO        |