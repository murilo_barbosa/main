#language: pt

@all @gestor @regras
Funcionalidade: Acessar a página inicial das Regras no Portal gestor


  @acessar_pagina_regras_cliente
  Cenário: No portal do Gestor, acessar a página de "Regras"
    Dado que esteja logado no produto "gestor" com o usuário "gestor-scob"
    Quando clicar no menu "Regras"
    Então a janela inicial das "Regras" do gestor deverá ser aberta

  @acessar_pagina_regras_admin
  Cenário: No portal do Gestor, acessar a página de "Regras"
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"
    Quando clicar no menu "Regras"
    Então a janela inicial das "Regras" do gestor deverá ser aberta

  @TODO
#  @visualizar_regras
#  Cenário: Na página de regras, visualizar detalhes de uma regra criada
#    E tenha uma regra válida criada
#    Quando clicar no botão "Ver Detalhe" da regra "válida" criada
#    Então a janela com os detalhes da regra "válida" deverá ser exibida

