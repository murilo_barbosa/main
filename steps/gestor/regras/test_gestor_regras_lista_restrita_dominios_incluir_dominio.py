import time
from behave import when, then
from features.utils import logger, constants, file_comum as fc
from features.steps.gestor.regras.functions.ListaRestritaDominios import ListaRestritaDominios

dominio = None
lista_restrita = ListaRestritaDominios()


@when(u'clicar no botão "Nova Restrição" da janela inicial "Lista Restrita Domínios"')
def clicar_botao_nova_restricao(context):
    global lista_restrita
    lista_restrita = ListaRestritaDominios()
    lista_restrita.clicar_botao_nova_restricao()


@when(u'clicar no botão "Salvar" na janela de "Nova Restrição"')
def clicar_botao_salvar(context):
    lista_restrita.clicar_botao_salvar()


@then(u'esteja na página incial "Nova Restrição"')
def acessar_pagina_nova_restricao(context):
    global lista_restrita
    lista_restrita = ListaRestritaDominios()
    lista_restrita.clicar_botao_configurar()
    lista_restrita.clicar_botao_nova_restricao()


@then(u'preencha o campo "Extensão Dominio" com "{domain}"')
def preencher_dominio(context, domain):
    if domain == 'mais de um dominio':
        domain = lista_restrita.multiplos_dominios
    lista_restrita.preencher_campo_dominio(domain)


@then(u'a página de "Nova Restrição" das regras deverá ser aberta')
def validar_pagina(context):
    texto_esperado = 'Nova Restrição'
    try:
        texto_retornado = lista_restrita.validar_pagina_nova_restricao()
        assert texto_esperado == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_retornado)
    except Exception as e:
        logger.logging.info(constants.ERRO_VALIDAR_PAGINA % texto_esperado)
        logger.logging.info(e)
        raise
    finally:
        lista_restrita.driver.quit()


@then(u'o dominio "{domain}" deverá ser criado com sucesso na janela "Lista Restrita Domínios"')
def validar_dominio_criado_sucesso(context, domain):
    time.sleep(1)
    janela = 'Lista Restrita Domínios'
    try:
        lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        result = fc.procurar_valor_lista_regras(lista, 'Extensões Domínios', domain)
        assert result
        logger.logging.info(constants.DOMINIO_CRIADO_SUCESSO % (domain, janela))
        lista_restrita.driver.save_screenshot(
            fc.name_screenshot_default(constants.DOMINIO_CRIADO_SUCESSO % (domain, janela)))
    except Exception as e:
        logger.logging.error(constants.ERRO_CADASTRAR_DOMINIO % (domain, janela))
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()


@then(u'os dominios criados deverão ser criados com sucesso na janela "Lista Restrita Domínios"')
def validar_mais_de_um_dominio(context):
    global dominio
    try:
        time.sleep(1)
        lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        for dominio in lista_restrita.multiplos_dominios.split(';'):
            assert fc.procurar_valor_lista_regras(lista, 'Extensões Domínios', dominio)
        logger.logging.info(
            constants.DOMINIO_CRIADO_SUCESSO % (lista_restrita.multiplos_dominios, 'Lista Restrita Domínios'))
        lista_restrita.driver.save_screenshot(fc.name_screenshot_default(
            constants.DOMINIO_CRIADO_SUCESSO % (lista_restrita.multiplos_dominios, 'Lista Restrita Domínios')))
    except Exception as e:
        logger.logging.error(constants.DOMINIO_NAO_ENCONTRADO % dominio)
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()
