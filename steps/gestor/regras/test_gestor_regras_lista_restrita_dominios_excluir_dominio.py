import time
from behave import given, when, then

from features.steps.gestor.regras.functions.ListaRestritaDominios import ListaRestritaDominios
from features.utils import logger, constants, file_comum as fc

lista_restrita = ListaRestritaDominios()
dominio = None


@given(u'selecionar um dominio para ser excluído')
def selecionar_um_dominio(context):
    global dominio, lista_restrita
    lista_restrita = ListaRestritaDominios()
    try:
        time.sleep(1)
        lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        if lista == 'VAZIO':
            lista_restrita.clicar_botao_nova_restricao()
            lista_restrita.preencher_campo_dominio(lista_restrita.multiplos_dominios)
            lista_restrita.clicar_botao_salvar()
            time.sleep(1)
        lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        dominio = lista[0]['Extensões Domínios']
        logger.logging.info(constants.SELECIONADO_DOMINIO % dominio)
    except Exception as e:
        logger.logging.error(e)
        raise


@when(u'clicar no botão "Excluir" da janela inicial "Lista Restrita Domínios"')
def clicar_botao_excluir(context):
    lista_restrita.clicar_botao_excluir()
    lista_restrita.clicar_botao_confirmar_remover_extensao()


@then(u'o dominio selecionado não deverá ser listado na janela "Lista Restrita Domínios"')
def validar_dominio_excluido_sucesso(context):
    global dominio
    janela = "Lista Restrita Domínios"
    time.sleep(1)
    try:
        lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        if lista == 'VAZIO':
            pass
        else:
            result = fc.procurar_valor_lista_regras(lista, 'Extensões Domínios', dominio)
            assert result is False
        logger.logging.info(constants.DOMINIO_EXCLUIDO_SUCESSO % (dominio, janela))
        lista_restrita.driver.save_screenshot(
            fc.name_screenshot_default(constants.DOMINIO_EXCLUIDO_SUCESSO % (dominio, janela)))
    except Exception as e:
        logger.logging.error(constants.ERRO_EXCLUIR_DOMINIO % dominio)
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()
