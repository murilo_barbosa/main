import time
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from features.utils import file_comum as fc, logger, constants


class Ddd(object):
    """Classe responsável por definir as regras de negócio do DDD."""

    def __init__(self):
        self.driver = fc.get_driver()
        self.id_sabado_modal_incluir = 'saturday'
        self.id_domingo_modal_incluir = 'sunday'
        self.id_sabado_modal_editar = 'saturdayEdit'
        self.id_domingo_modal_editar = 'sundayEdit'
        self.dia_sabado = 'Sábado'
        self.dia_domingo = 'Domingo'
        self.dia_semana = 'Semana'
        self.texto_estado = 'Estado'
        self.texto_ddd = 'DDD'
        self.texto_botao_desabilitar = 'Desabilitar'
        self.texto_botao_habilitar = 'Habilitar'
        self.texto_botao_configurar = 'Configurar'
        self.wait = WebDriverWait(self.driver, 5)

    def acrescentar_hora_periodo(self, dia):
        """
        Acrescenta uma hora a mais.
        Faz a validação quando passar das 00:00 para o próximo dia.
        Inclui o número 0 (zero) a esquerda para manter 2 digitos.
        """
        try:
            hora_inicio_html, hora_fim_html = self.pegar_horario_primeira_linha(dia)
            hora_inicio = int(hora_inicio_html) + 1
            hora_fim = int(hora_fim_html) + 1
            if hora_inicio > 23 or hora_fim > 23:
                hora_inicio = 1
                hora_fim = 8
            return str(hora_inicio).rjust(2, '0'), str(hora_fim).rjust(2, '0')
        except Exception as e:
            logger.logging.error(e)

    def alterar_ordenacao(self, de, para):
        """Alterar o tipo de ordenação, de DDD para Estado e Estado para DDD"""
        text_de_para = constants.ORDENACAO_ALTERADA_DE_PARA % (de, para)
        try:
            if de == self.texto_estado and para == self.texto_ddd:
                self.driver.find_element(By.LINK_TEXT, self.texto_ddd).click()
                self.driver.save_screenshot(fc.name_screenshot_default(text_de_para))
            elif de == self.texto_ddd and para == self.texto_estado:
                element_ordenar_por = '/html/body/section/div/div[2]/div[2]/div/div[3]/div[1]/div/div[' \
                                      '1]/div/div/div/button/span[1] '
                text = self.driver.find_element(By.XPATH, element_ordenar_por).text
                if text == para:
                    self.driver.find_element(By.LINK_TEXT, self.texto_ddd).click()
                    logger.logging.info(constants.ORDENACAO_ALTERADA_DE_PARA % (para, de))
                    self.clicar_dropdown_ordenar_por()
                self.driver.find_element(By.LINK_TEXT, self.texto_estado).click()
                self.driver.save_screenshot(fc.name_screenshot_default(text_de_para))
            logger.logging.info(text_de_para)
        except Exception as e:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % e)
            raise

    def alterar_periodo(self, dia, hora_inicio, hora_final):
        """Editar a hora inicial e hora final do período da semana"""
        self.wait.until(EC.visibility_of(self.driver.find_element(By.XPATH, "(//input[@id=\'startTime\'])[2]")))
        try:
            match dia:
                case self.dia_semana:
                    elemento_hora_inicio = self.driver.find_element(By.XPATH, "(//input[@id=\'startTime\'])[2]")
                    elemento_hora_final = self.driver.find_element(By.XPATH, "(//input[@id=\'finalTime\'])[2]")
                case self.dia_sabado:
                    elemento_hora_inicio = self.driver.find_element(By.XPATH, "(//input[@id=\'startTimeSab\'])[2]")
                    elemento_hora_final = self.driver.find_element(By.XPATH, "(//input[@id=\'finalTimeSab\'])[2]")
                case self.dia_domingo:
                    elemento_hora_inicio = self.driver.find_element(By.XPATH, "(//input[@id=\'startTimeDom\'])[2]")
                    elemento_hora_final = self.driver.find_element(By.XPATH, "(//input[@id=\'finalTimeDom\'])[2]")
                case _:
                    logger.logging.error(f'Dia {dia} não encontrado.')
                    raise
            elemento_hora_inicio.clear()
            elemento_hora_inicio.send_keys(str(hora_inicio))
            logger.logging.info(constants.ALTERANDO_HORA_INICIO % hora_inicio)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.ALTERANDO_HORA_INICIO % hora_inicio))
            elemento_hora_final.clear()
            elemento_hora_final.send_keys(str(hora_final))
            logger.logging.info(constants.ALTERANDO_HORA_FINAL % hora_final)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.ALTERANDO_HORA_FINAL % hora_final))
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def clicar_botao_configurar_ddd(self):
        """Faz o clique no botão configurar DDD."""
        xpath = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[20]/div/button[1]'
        btn_name = 'Configurar'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.XPATH, xpath)))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            self.driver.find_element(By.XPATH, xpath).click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)

    def clicar_dropdown_ordenar_por(self):
        """Clicar no dropdown para selecionar o tipo de ordenação das regras"""
        dropdown = 'Ordenar por:'
        elemento = 'btn.btn-default.dropdown-toggle.as-is.bs-dropdown-to-select'
        try:
            time.sleep(1)
            self.driver.find_element(By.CLASS_NAME, elemento).click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % dropdown)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % dropdown))
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown)
            logger.logging.error(e)
            raise

    def clicar_botao_reordenar(self):
        """"Clicar no botão reordenar para alterar a ordenação de decrescente para crescente e vise-versa"""
        botao = 'Reordenar'
        elemento = 'btn.btn-default.top-10'
        try:
            self.driver.find_element(By.CLASS_NAME, elemento).click()
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_desabilitar_regra(self):
        """Clica no botão desabilitar a regra de DDD"""
        try:
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.CLICANDO_BOTAO % self.texto_botao_desabilitar))
            self.driver.find_element(By.LINK_TEXT, self.texto_botao_desabilitar).click()
            logger.logging.info(constants.CLICANDO_BOTAO % self.texto_botao_desabilitar)
            time.sleep(1)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_desabilitar)
            raise

    def clicar_botao_habilitar_regra(self):
        """Clica no botão habilitar a regra de DDD"""
        elemento = 'btn.btn-success.pull-right'
        try:
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.CLICANDO_BOTAO % self.texto_botao_habilitar))
            self.driver.find_element(By.CLASS_NAME, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % self.texto_botao_habilitar)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_habilitar)
            logger.logging.error(e)
            raise

    def clicar_botao_confirmar_desabilitar_regra(self):
        """Clica no botão do modal para confirmar a desabilitar a Regra DDD"""
        texto_botao_confirmar = 'Desabilitar Regra'
        elemento = 'btn.btn-danger'
        time.sleep(1)
        try:
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.CLICANDO_BOTAO % texto_botao_confirmar))
            self.driver.find_element(By.CLASS_NAME, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % texto_botao_confirmar)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % texto_botao_confirmar)
            raise

    def clicar_botao_confirmar_habilitar_regra(self):
        """Clica no botão do modal para confirmar a habilitar a Regra DDD"""
        texto_botao_confirmar = 'Habilitar Regra'
        elemento = '#modalConfirmEnable button.btn.btn-success'
        time.sleep(1)
        try:
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.CLICANDO_BOTAO % texto_botao_confirmar))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % texto_botao_confirmar)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % texto_botao_confirmar)
            raise

    def clicar_botao_editar_regra(self):
        """Clicar no botão editar uma regra de DDD"""
        nome_botao = 'Editar'
        elemento = '#listItem button.orange'
        time.sleep(1)
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % nome_botao))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % nome_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % nome_botao)
            logger.logging.error(e)
            raise

    def clicar_botao_salvar(self):
        """Clicando no botão Salvar as edições do modal Editar Regra por DDD"""
        texto_botao = 'Salvar'
        elemento = '#editDddModal button.btn'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % texto_botao))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % texto_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % texto_botao)
            logger.logging.error(e)
            raise

    def clicar_botao_novo_ddd(self):
        """Clicar no botão para incluir um novo DDD."""
        time.sleep(1)
        botao = '+ Novo DDD'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            self.driver.find_element(By.CLASS_NAME, 'btn.btn-success.pull-right.ng-scope').click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_incluir_regra(self):
        """Clicar no botão Incluir regra"""
        botao = 'Incluir'
        elemento = '//*[@id="newDddModal"]/div[2]/div/div[4]/button'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            self.driver.find_element(By.XPATH, elemento).click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_cancelar_excluir_regra(self):
        """Clicar no botão cancelar do modal de confirmação para excluir uma regra"""
        xpath = '//*[@id="removeRuleModal"]/div[2]/div/div[2]/div/button[1]'
        btn_name = 'Cancelar'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.XPATH, xpath)))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            self.driver.find_element(By.XPATH, xpath).click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def clicar_botao_excluir_regra(self, screenshot=True):
        """Clicar no botão Excluir Regra"""
        xpath = '//*[@id="listItem"]/div[6]/button[2]'
        btn_name = 'Excluir'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'tempContainer')))
            time.sleep(0.2)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            self.driver.find_element(By.XPATH, xpath).click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
            time.sleep(1)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def clicar_botao_confirmar_remover_regra(self, screenshot=True):
        """Incluir no botão para confirmar a remoção da regra"""
        xpath = '//*[@id="removeRuleModal"]/div[2]/div/div[2]/div/button[2]'
        btn_name = 'Remover regra'
        try:
            if screenshot:
                self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            self.driver.find_element(By.XPATH, xpath).click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.CLICANDO_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def incluir_hora(self, dia, hora_inicio, hora_final):
        """Incluir Hora no modal Incluir Nova Regra"""
        try:
            if dia == self.dia_semana:
                id_hora_inicio = 'startTime'
                id_hora_final = 'finalTime'
            elif dia == self.dia_sabado:
                id_hora_inicio = 'startTimeSab'
                id_hora_final = 'finalTimeSab'
            elif dia == self.dia_domingo:
                id_hora_inicio = 'startTimeDom'
                id_hora_final = 'finalTimeDom'
            else:
                id_hora_inicio = None
                id_hora_final = None
            self.driver.find_element(By.ID, id_hora_inicio).send_keys(hora_inicio)
            logger.logging.info(constants.ALTERANDO_HORA_INICIO % hora_inicio)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.ALTERANDO_HORA_INICIO % hora_inicio))
            self.driver.find_element(By.ID, id_hora_final).send_keys(hora_final)
            logger.logging.info(constants.ALTERANDO_HORA_FINAL % hora_final)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.ALTERANDO_HORA_FINAL % hora_final))
        except Exception as e:
            logger.logging.error(e)
            raise

    def modal_editar_regra_ddd(self):
        """Mover o seletor para dentro do modal de editar periodo"""
        time.sleep(1)
        elemento = 'editDddModal'
        try:
            self.driver.find_element(By.ID, elemento).click()
            element = self.driver.find_element(By.CSS_SELECTOR, "body")
            actions = ActionChains(self.driver)
            actions.move_to_element(element).perform()
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def pegar_texto_janela_ddd(self):
        css_selector = 'div[class="col-xs-12 col-md-6"] > h1'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.CSS_SELECTOR, css_selector)))
            text = self.driver.find_element(By.CSS_SELECTOR, css_selector).text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)

    def pegar_tabela_regras_ddd(self):
        """Pega todos os dados da tabela HTML com as regras de DDD ja criadas"""
        try:
            html = self.driver.page_source
            soup = BeautifulSoup(html, 'html.parser')
            titulos_html = soup.find('div', {'class': 'tableGrid header-table'}).find_all('p')
            valores_html = soup.find('div', {'id': 'tempContainer'}).find_all('p', {'class': 'ng-binding'})
            lista_titulos = [titulos.getText().replace('<p>', "") for titulos in titulos_html]
            lista_valores = [valores.getText().replace('<p>', "") for valores in valores_html]
            lista = []
            cont = 0
            while cont < len(lista_valores):
                for valor in [lista_valores[cont:]]:
                    dicionario = dict(zip(lista_titulos[:-1], valor))
                    lista.append(dicionario)
                cont += 5
            return lista
        except Exception as e:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % e)
            raise

    def pegar_texto_modal_editar_ddd(self):
        """Validar se realmente é o modal de editar DDD"""
        elemento = '//*[@id="editDddModal"]/div[2]/div/div[1]/h4'
        self.wait.until(EC.visibility_of(self.driver.find_element(By.XPATH, elemento)))
        try:
            texto = self.driver.find_element(By.XPATH, elemento).text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % texto)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto))
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def pegar_texto_modal_incluir_ddd(self):
        """Validar se realmente é o modal de incluir novo DDD."""
        elemento = '//*[@id="newDddModal"]/div[2]/div/div[1]/h4'
        time.sleep(1)
        try:
            texto = self.driver.find_element(By.XPATH, elemento).text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % texto)
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def pegar_horario_primeira_linha(self, dia):
        """Pegar o horário da semana da primeira linha da tabela de regras DDD criada."""
        self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'tempContainer')))
        time.sleep(0.2)
        try:
            dados = self.pegar_tabela_regras_ddd()[0]
            hora_inicio_tabela = dados[dia][0:2]
            hora_fim_tabela = dados[dia][9:11]
            if dados[dia] == ':00 às :00':
                self.selecionar_checkbox_modal_editar(dia)
                hora_inicio_tabela = "0"
                hora_fim_tabela = "8"
            logger.logging.info(dados)
            return hora_inicio_tabela, hora_fim_tabela
        except Exception as e:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % dia)
            logger.logging.error(e)

    def pegar_texto_modal_regra_duplicada(self):
        """Pegando o texto do modal regra duplicada para validação"""
        time.sleep(1)
        try:
            texto = self.driver.find_element(By.ID, 'odinErrorMessage').text
            self.driver.find_element(By.XPATH, '//*[@id="odinModalError"]/div[2]/div/div[3]/button').click()
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)
            raise

    def realizar_busca_ddd(self, valor_busca):
        """Faz um filtro por Estado ou DDD na tela incial da regra de DDD"""
        campo = 'Busca'
        try:
            self.driver.find_element(By.ID, 'findInput').send_keys(valor_busca)
            logger.logging.info(constants.DIGITANDO_TEXTO_CAMPO % (valor_busca, campo))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.DIGITANDO_TEXTO_CAMPO % (valor_busca, campo)))
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO % campo)
            logger.logging.error(e)

    def selecionar_checkbox_modal_editar(self, dia):
        """
        Marca/Desmarca o checkbox conforme o dia, sabado ou domingo.
        """
        try:
            if dia == self.dia_sabado:
                id_elemento = self.id_sabado_modal_editar
            elif dia == self.dia_domingo:
                id_elemento = self.id_domingo_modal_editar
            else:
                id_elemento = None
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_CHECKBOX % dia))
            self.driver.find_element(By.ID, id_elemento).click()
            logger.logging.info(constants.CLICANDO_CHECKBOX % dia)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_CHECKBOX % dia)
            logger.logging.error(e)
            raise

    def selecionar_checkbox_modal_incluir(self, dia):
        """
        Marca/Desmarca o checkbox conforme o dia, sabado ou domingo.
        """
        try:
            if dia == self.dia_sabado:
                id_elemento = self.id_sabado_modal_incluir
            elif dia == self.dia_domingo:
                id_elemento = self.id_domingo_modal_incluir
            else:
                id_elemento = None
            self.driver.find_element(By.ID, id_elemento).click()
            logger.logging.info(constants.CLICANDO_CHECKBOX % dia)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_CHECKBOX % dia))
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_CHECKBOX % dia)
            logger.logging.error(e)
            raise

    def selecionar_ddd(self, estado, codigo_ddd):
        """Selecionar um estado e DDD conforme o estado selecionado."""
        try:
            time.sleep(1)
            self.driver.find_element(By.ID, 'stateBtn').click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % self.texto_estado)
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % self.texto_estado))
            self.driver.find_element(By.LINK_TEXT, estado).click()
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (estado, self.texto_estado))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (estado, self.texto_estado)))
            self.driver.find_element(By.ID, f'{codigo_ddd}ddd').click()
            logger.logging.info(constants.CLICANDO_CHECKBOX % codigo_ddd)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_CHECKBOX % codigo_ddd))
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % self.texto_estado)
            logger.logging.error(e)
            raise

    def validar_janela_confirmacao_desabilitar_habilitar_regra(self, type_model):
        """
        Validação se o modal de confirmação esta conforme o botão selecionado (Habilitar ou Desabilitar) da regra
        """
        try:
            if type_model == self.texto_botao_desabilitar:
                modal = self.driver.find_element(By.XPATH,
                                                 '//*[@id="modalConfirmDisable"]/div[2]/div/div[1]/h4/span').text
            elif type_model == self.texto_botao_habilitar:
                time.sleep(1)
                modal = self.driver.find_element(By.XPATH,
                                                 '//*[@id="modalConfirmEnable"]/div[2]/div/div[1]/h4/span').text
            else:
                modal = None
            return modal
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)
            raise

    def validar_status_regra(self, status):
        """Validar se a regra foi realmente Habilitada/Desabilitada"""
        time.sleep(2)
        try:
            if status == 'Desabilitada':
                self.driver.find_element(By.XPATH,
                                         '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[20]/a')
            elif status == 'Habilitada':
                self.driver.find_element(By.XPATH,
                                         '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[19]/div/button[2]/span')
            self.driver.save_screenshot(fc.name_screenshot_default(constants.VALIDAR_REGRA % status))
            return status
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def verificar_status_checkbox_modal_editar_regra(self, dia):
        """Verificar se o checkbox do modal EDITAR sabado/domingo esta marcado ou desmarcado."""
        try:
            if dia == self.dia_sabado:
                id_elemento = self.id_sabado_modal_editar
            elif dia == self.dia_domingo:
                id_elemento = self.id_domingo_modal_editar
            else:
                id_elemento = None
            return self.driver.find_element(By.ID, id_elemento).is_selected()
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise

    def verificar_status_checkbox_modal_incluir_regra(self, dia):
        """Verificar se o checkbox do modal INCLUIR sabado/domingo esta marcado ou desmarcado."""
        try:
            if dia == self.dia_sabado:
                id_elemento = self.id_sabado_modal_incluir
            elif dia == self.dia_domingo:
                id_elemento = self.id_domingo_modal_incluir
            else:
                id_elemento = None
            return self.driver.find_element(By.ID, id_elemento).is_selected()
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_ELEMENTO)
            logger.logging.error(e)
            raise
