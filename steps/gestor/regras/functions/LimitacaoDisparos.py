import time

from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import Select
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from features.utils import file_comum as fc, logger, constants
from faker import Faker


class LimitacaoDisparos(object):
    def __init__(self):
        self.driver = fc.get_driver()
        self.text_initial_page = 'Limitação de disparos'
        self.wait = WebDriverWait(self.driver, 5, poll_frequency=1)
        self.text = None

    def click_button_configurar(self):
        xpath = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div[20]/div/button[1]'
        btn_name = 'Configurar'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_desabilitar(self):
        xpath = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div[20]/a'
        btn_name = 'Desabilitar'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_habilitar(self):
        xpath = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div[19]/div/button[2]'
        btn_name = 'Habilitar'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_habilitar_regra(self):
        xpath = '//*[@id="modalConfirmEnable"]/div[2]/div/div[3]/button[2]'
        btn_name = 'Habilitar Regra'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_desabilitar_regra(self):
        xpath = '//*[@id="modalConfirmDisable"]/div[2]/div/div[3]/button[2]'
        btn_name = 'Desabilitar Regra'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_edit(self):
        btn_name = 'Editar'
        xpath = '//*[@id="listItem"]/div[3]/button[1]'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'listContainer')))
            element = self.driver.find_element(By.XPATH, xpath)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
            time.sleep(1)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_button_reorder(self):
        css_selector = 'button[class="btn btn-default top-10"]'
        btn_name = 'Reordenar'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'tempContainer')))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % btn_name))
            self.driver.find_element(By.CSS_SELECTOR, css_selector).click()
            logger.logging.info(constants.CLICANDO_BOTAO % btn_name)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_name)
            logger.logging.error(e)
            raise

    def click_dropdown(self):
        xpath = '/html/body/section/div/div[2]/div[2]/div/div[3]/div[2]/div/div[1]/div/div/div/button'
        dropdown_name = 'Ordenar por'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.XPATH, xpath)))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % dropdown_name))
            self.driver.find_element(By.XPATH, xpath).click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % dropdown_name)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown_name)
            raise

    def click_button_add_new_rule(self):
        name_button = 'Adicionar Nova Regra'
        try:
            element = self.driver.find_element(By.ID, 'addRow')
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % name_button))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % name_button)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % name_button)
            logger.logging.error(e)
            raise

    def click_button_save_new_rule(self):
        name_button = 'Salvar'
        css_selector = 'button[class="btn btn-success ng-scope"]'
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, css_selector)
            self.wait.until(EC.element_to_be_clickable(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % name_button))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % name_button)
            time.sleep(1)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % name_button)
            logger.logging.error(e)
            raise

    def click_button_confirmar(self):
        css_selector = 'button[class="btn btn-success"]'
        name_button = 'Confirmar'
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, css_selector)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % name_button))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % name_button)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % name_button)
            logger.logging.error(e)
            raise

    def click_button_cancelar(self):
        css_selector = 'button[class="btn btn-success"]'
        name_button = 'Confirmar'
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, css_selector)
            self.wait.until(EC.visibility_of(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % name_button))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % name_button)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % name_button)
            logger.logging.error(e)
            raise

    def confirm_option_selected(self):
        xpath = '//*[@id="newLimit"]/div[3]/button[2]'
        icon_button = 'Ícone Confirmar'
        try:
            element = self.driver.find_element(By.XPATH, xpath)
            self.wait.until(EC.element_to_be_clickable(element))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % icon_button))
            element.click()
            logger.logging.info(constants.CLICANDO_BOTAO % icon_button)
        except Exception as e:
            logger.logging.error(constants.CLICANDO_BOTAO % icon_button)
            logger.logging.error(e)
            raise

    def edit_name_text(self, name_fake=None):
        """Caso o deseje gerar um nome Fake automaticamnete nada fazer.
        Caso deseje criar um nome, entrar com o nome na Variavel name_fake."""
        label = 'Nome'
        try:
            if name_fake is None:
                self.text = Faker().name()
            else:
                self.text = name_fake
            element = self.driver.find_element(By.ID, 'ruleName')
            self.wait.until(EC.visibility_of(element))
            element.clear()
            element.send_keys(self.text)
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.DIGITANDO_TEXTO_CAMPO % (self.text, label)))
            logger.logging.info(constants.DIGITANDO_TEXTO_CAMPO % (self.text, label))
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_CAMPO % label)
            logger.logging.error(e)

    def set_quantity(self, quantity=1):
        name_imput = 'Quantidade'
        try:
            element = self.driver.find_element(By.ID, 'newLimitQuantity')
            element.send_keys(str(quantity))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.DIGITANDO_TEXTO_CAMPO % (quantity, name_imput)))
            logger.logging.info(constants.DIGITANDO_TEXTO_CAMPO % (quantity, name_imput))
        except Exception as e:
            logger.logging.error(e)

    def select_option_new_rule(self, text_option):
        name_new_rule = 'Escolha Uma Regra'
        try:
            element = self.driver.find_element(By.ID, 'newLimitType')
            self.wait.until(EC.visibility_of(element))
            select = Select(element)
            select.select_by_visible_text(text_option)
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (text_option, name_new_rule)))
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (text_option, name_new_rule))
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % text_option)
            logger.logging.error(e)
            raise

    def select_option_carteira(self, text_option):
        dropdown_name = 'Carteira'
        try:
            element = self.driver.find_element(By.ID, 'ruleWallet')
            self.wait.until(EC.visibility_of(element))
            select = Select(element)
            select.select_by_visible_text(text_option)
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (text_option, dropdown_name)))
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (text_option, dropdown_name))
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % text_option)
            logger.logging.error(e)
            raise

    def filter_option(self, text_filter):
        name_field = 'Busca'
        try:
            element = self.driver.find_element(By.ID, 'findInput')
            self.wait.until(EC.visibility_of(element))
            element.send_keys(text_filter)
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.DIGITANDO_TEXTO_CAMPO % (text_filter, name_field)))
            element.send_keys(Keys.ENTER)
            logger.logging.info(constants.DIGITANDO_TEXTO_CAMPO % (text_filter, name_field))
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def select_option_dropdown(self, link_text):
        dropdown_name = 'Ordenar por'
        try:
            element = self.driver.find_element(By.LINK_TEXT, link_text)
            self.wait.until(EC.element_to_be_clickable(element))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (link_text, dropdown_name)))
            element.click()
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (link_text, dropdown_name))
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % link_text)
            logger.logging.error(e)
            raise

    def get_text_mesage_warning_quantity(self):
        try:
            element = self.driver.find_element(By.ID, 'newLimitQuantityError')
            self.wait.until(EC.visibility_of(element))
            text = element.text
            self.driver.save_screenshot(fc.constants.TEXTO_ENCONTRADO_SUCESSO % text)
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)
            raise

    def get_text_mesage_warning_rule(self):
        try:
            element = self.driver.find_element(By.ID, 'newLimitTypeError')
            self.wait.until(EC.visibility_of(element))
            text = element.text
            self.driver.save_screenshot(fc.constants.TEXTO_ENCONTRADO_SUCESSO % text)
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)
            raise

    def get_text_mesage_warning_name(self):
        try:
            element = self.driver.find_element(By.ID, 'ruleNameError')
            self.wait.until(EC.visibility_of(element))
            text = element.text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)
            raise

    def get_text_inicial_page(self):
        css_selector = 'div[class="col-xs-12 col-md-6"] > h1'
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, css_selector)
            self.wait.until(EC.visibility_of(element))
            text = element.text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO % self.text_initial_page)
            raise

    def get_text_initial_edit_page(self):
        xpath = '/html/body/section/div/div[2]/div[2]/div/div[1]/div/h1[1]'
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'listContainer')))
            text = self.driver.find_element(By.XPATH, xpath).text
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % text)
            return text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
            logger.logging.error(e)

    def get_table_page_editar_limitacao_disparos(self):
        """Pega todos os dados da tabela HTML com as regras de Estratégia ja criadas"""
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'tempContainer')))
            html = self.driver.page_source
            soup = BeautifulSoup(html, 'html.parser')
            titulos_html = soup.find('div', {'class': 'shotTableGrid header-table'}).find_all('p')
            valores_html = soup.find('div', {'id': 'listContainer'}).find_all('p', {'class': 'ng-binding'})
            lista_titulos = [titulos.getText().replace('<p>', "") for titulos in titulos_html]
            lista_valores = [valores.getText().replace('<p>', "") for valores in valores_html]
            lista = []
            cont = 0
            while cont < len(lista_valores):
                for valor in [lista_valores[cont:]]:
                    dicionario = dict(zip(lista_titulos[:-1], valor))
                    lista.append(dicionario)
                cont += 2
            lista = fc.remove_dict_dupicate_in_list(lista)
            return lista
        except Exception as e:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % e)
            raise

    def validate_rule(self, rule):
        time.sleep(1)
        try:
            match rule:
                case 'Desabilitada':
                    return self.click_button_desabilitar()
                case 'Habilitada':
                    return self.click_button_habilitar()
                case _:
                    logger.logging.error(constants.VALOR_NAO_ENCONTRADO % rule)
                    raise
        except Exception:
            return None

    def validate_order_list(self):
        self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'tempContainer')))
