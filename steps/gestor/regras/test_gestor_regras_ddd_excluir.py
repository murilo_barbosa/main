from behave import given, when, then
import time
from features.steps.gestor.regras.functions.Ddd import Ddd
from features.utils import logger, constants, file_comum as fc

ddd = Ddd()
hora_inicio = '08'
hora_final = '18'
estado = 'Acre'
cod_ddd = '68'


@given(u'tenha uma regra de DDD previamente cadastrada')
def regra_cadastrada(context):
    global ddd
    ddd = Ddd()
    ddd.clicar_botao_configurar_ddd()
    ddd.clicar_botao_novo_ddd()
    time.sleep(1)
    ddd.selecionar_ddd(estado, cod_ddd)
    ddd.incluir_hora(ddd.dia_semana, hora_inicio, hora_final)
    ddd.clicar_botao_incluir_regra()
    time.sleep(1)


@when(u'clicar no botão excluir da página de "DDD"')
def clicar_botao_excluir(context):
    ddd.clicar_botao_excluir_regra()


@when(u'confirmar clicando no botão "Remover regra"')
def clicar_botao_confirmar(context):
    ddd.clicar_botao_confirmar_remover_regra()


@then(u'a regra de "DDD" deverá ser excluida com sucesso')
def validar_se_regra_foi_excluida(context):
    try:
        time.sleep(3)
        lista_regras = ddd.pegar_tabela_regras_ddd()
        lista_ddd = fc.procurar_valor_lista_regras(lista_regras, "DDD", cod_ddd)
        lista_estado = fc.procurar_valor_lista_regras(lista_regras, "Estado", estado)
        assert lista_ddd is False and lista_estado is False
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % 'DDD')
        ddd.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % 'DDD'))
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % 'DDD')
        logger.logging.error(e)
        raise
    finally:
        ddd.driver.quit()
