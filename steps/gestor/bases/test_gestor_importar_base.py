import os.path
import shutil
from behave import when, then
from features.steps.gestor.bases.functions.ImportarBase import ImportarBase
from features.support.Browser import Browser
from features.utils import constants, logger, file_comum as fc

importar_base = ImportarBase()
diretorio_raiz_capturas = 'gestor/bases'


@when(u'preencher os dados para importar a base com "sucesso"')
def importar_base_sucesso(context):
    global importar_base
    importar_base = ImportarBase()
    importar_base.access_bases()
    importar_base.click_btn_new_base()
    importar_base.set_values_name()
    importar_base.set_values_account()
    importar_base.set_values_format()
    importar_base.upload_base()


@when(u'clicar no botão "{button}" na janela de importar a base')
def click_button(context, button):
    match button:
        case 'Salvar':
            importar_base.click_btn_save()
        case 'Cancelar':
            importar_base.click_btn_cancel()


@when(u'digitar o nome da base a ser buscada')
def set_nome_base_a_ser_buscada(context):
    global importar_base
    importar_base = ImportarBase()
    importar_base.access_bases()
    importar_base.search_base_selected()


@when(u'clicar no botão "{condition}" de uma base importada')
def clicar_botao_detalhes(context, condition):
    global importar_base
    importar_base = ImportarBase()
    importar_base.access_bases()
    if condition == 'Detalhes':
        importar_base.click_btn_details()
    elif condition == 'Download':
        importar_base.click_btn_download("Bases")


@when(u'clicar no checkBox "{origin}" da origem de uma base importada')
def clicar_check_box_origem(context, origin):
    global importar_base
    importar_base = ImportarBase()
    importar_base.access_bases()
    importar_base.select_origin(origin)


@then(u'a base deve ser importada com sucesso')
def validar_se_base_foi_importada(context):
    try:
        context.id_nome_gerado = str(importar_base.texto_nome)
        context.id_nome_tabela = str(importar_base.get_name_base())
        assert context.id_nome_gerado == context.id_nome_tabela
        logger.logging.info(constants.BASE_VALIDADA % context.id_nome_gerado)
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_BASE_IMPORTADA % (context.id_nome_gerado, context.id_nome_tabela))
        raise
    finally:
        importar_base.driver.save_screenshot(fc.name_screenshot_default('Base importada'))
        importar_base.driver.quit()


@then(u'a base não deve ser importada')
def validar_se_base_nao_foi_importada(context):
    try:
        context.id_nome_gerado = str(importar_base.texto_nome)
        context.id_nome_tabela = str(importar_base.get_name_base())
        assert context.id_nome_gerado != context.id_nome_tabela
        logger.logging.info(constants.BASE_NAO_VALIDADA % context.id_nome_gerado)
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_BASE_IMPORTADA % (context.id_nome_gerado, context.id_nome_tabela))
        raise
    finally:
        importar_base.driver.save_screenshot(
            fc.name_screenshot_default(constants.BASE_NAO_VALIDADA % context.id_nome_gerado))
        importar_base.driver.quit()


@then(u'a base buscada deve ser a única listada')
def validar_base_listada(context):
    global importar_base
    try:
        context.quantidade_base_listada = len(importar_base.get_table_html_bases_imported_json())
        assert context.quantidade_base_listada == 1
        logger.logging.info('A base foi listada com sucesso!')
    except Exception:
        logger.logging.error(constants.ERRO_COMPARACAO_MENSAGEM % (1, context.quantidade_base_listada))
        raise
    finally:
        importar_base.driver.save_screenshot(fc.name_screenshot_default('A base foi listada com sucesso!'))
        importar_base.driver.quit()


@then(u'a janela de detalhes deve ser exibida')
def validar_se_janela_detalhes_foi_exibida(context):
    global importar_base
    texto_esperado = 'Detalhes da Base de dados'
    texto_pagina = importar_base.get_text_data_imported()
    try:
        assert texto_pagina == texto_esperado
        logger.logging.info('Pagina Detalhes carregada com sucesso!')
    except Exception:
        logger.logging.error(constants.ERRO_COMPARACAO_MENSAGEM % (texto_esperado, texto_pagina))
        raise
    finally:
        importar_base.driver.quit()


@then(u'o filtro da base importada "{origin}" deve ser listada')
def validar_filtro_base_listada(context, origin):
    global importar_base
    try:
        context.origem_retornada = importar_base.get_table_html_bases_imported_json()[0]['Origem']
        assert context.origem_retornada == origin
        logger.logging.info(f'A origem "{context.origem_retornada}" foi validada com sucesso.')
    except Exception:
        logger.logging.error(constants.ERRO_COMPARACAO_MENSAGEM % (origin, context.origem_retornada))
        raise
    finally:
        importar_base.driver.save_screenshot(
            fc.name_screenshot_default(f'A origem "{context.origem_retornada}" foi validada com sucesso.'))
        importar_base.driver.quit()


@then(u'o download deverá ser realizado com sucesso')
def validar_download_realizado(context):
    try:
        diretorio = Browser(None).path_default_downloads
        caminhos = [os.path.join(diretorio, nome) for nome in os.listdir(diretorio)]
        assert len(caminhos) > 0
        logger.logging.info(constants.DOWNLOAD_VALIDADO_SUCESSO)
        shutil.rmtree(diretorio)
        logger.logging.info(constants.EXCLUINDO_ARQUIVOS_TESTE)
        importar_base.driver.save_screenshot(fc.name_screenshot_default(constants.DOWNLOAD_VALIDADO_SUCESSO))
    except Exception as e:
        logger.logging.error(constants.ERRO_GENERICO % e)
        raise
    finally:
        importar_base.driver.quit()
