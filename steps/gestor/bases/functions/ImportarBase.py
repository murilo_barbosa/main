import os.path
import random
import time
import pyautogui
from selenium import webdriver
from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from bs4 import BeautifulSoup
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from features.utils import logger, constants, file_comum as fc


class ImportarBase(object):
    """Classe responsável por importar uma base"""

    def __init__(self):
        self.driver = fc.get_driver()
        self.texto_nome = None

    def access_bases(self):
        """Clicando no link do menu 'Bases' para direcionar a tela de importar base"""
        link_text_base = 'Bases'
        click_menu = constants.CLICANDO_MENU % link_text_base
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(click_menu))
            self.driver.find_element(By.LINK_TEXT, link_text_base).click()
            logger.logging.info(click_menu)
            time.sleep(2)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_MENU % link_text_base)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def click_btn_new_base(self):
        """Clicando no botão 'Nova base' """
        btn_nova_base = ".btn-success"
        click_button = constants.CLICANDO_BOTAO % 'Nova Base'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(click_button))
            self.driver.find_element(By.CSS_SELECTOR, btn_nova_base).click()
            logger.logging.info(click_button)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def set_values_name(self):
        """Inserindo um nome para a base. Neste caso, o nome terá um 'id' único."""
        time.sleep(1)
        id_nome = fc.gerar_id_unico_com_datetime()
        self.texto_nome = f'TestesAutomatizados: {id_nome}'
        text_label_name = 'Nome'
        name = 'name'
        field_text = constants.DIGITANDO_TEXTO_CAMPO % (self.texto_nome, text_label_name)
        try:
            element = self.driver.find_element(By.NAME, name)
            webdriver.ActionChains(self.driver).move_to_element(element).click().perform()
            logger.logging.info(constants.CLICANDO_CAMPO % text_label_name)
            element.send_keys(self.texto_nome)
            self.driver.save_screenshot(fc.name_screenshot_default(field_text))
            logger.logging.info(field_text)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_CAMPO % text_label_name)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def set_values_account(self):
        """Selecionando a conta para importar a base"""
        time.sleep(1)
        label_account = 'Conta'
        option_select_account = 'escob1'
        account = 'account'
        dropdown = constants.SELECIONADO_OPCAO_DROPDOWN % (option_select_account, label_account)
        try:
            element = self.driver.find_element(By.NAME, account)
            element.click()
            logger.logging.info(constants.CLICANDO_CAMPO % label_account)
            select_conta = Select(element)
            select_conta.select_by_visible_text(option_select_account)
            self.driver.save_screenshot(fc.name_screenshot_default(dropdown))
            logger.logging.info(dropdown)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def set_values_format(self):
        """Selecionando o formato para importar a base"""
        time.sleep(1)
        label_format = 'Formato'
        option_select_format = 'BvFormat'
        format = 'format'
        dropdown = constants.SELECIONADO_OPCAO_DROPDOWN % (option_select_format, label_format)
        try:
            element = self.driver.find_element(By.NAME, format)
            select_formato = Select(element)
            select_formato.select_by_visible_text(option_select_format)
            self.driver.save_screenshot(fc.name_screenshot_default(dropdown))
            logger.logging.info(dropdown)
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % label_format)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def upload_base(self):
        """Fazendo o upload do arquivo contendo a base a ser importada"""
        time.sleep(1)
        el_id_dropezone = 'newBaseDropzone'
        text_success = 'span[class="m-l-5"]'

        try:
            element = self.driver.find_element(By.ID, el_id_dropezone)
            caminho_csv_base = os.path.abspath(os.getcwd() + '/features/support/arquivos/model_base.csv')
            webdriver.ActionChains(self.driver).move_to_element(element).click().perform()
            logger.logging.info(constants.CLICANDO_PARA_UPLOAD_ARQUIVO % 'dropzone')

            time.sleep(1)
            pyautogui.write(caminho_csv_base)
            time.sleep(1)
            logger.logging.info(constants.LOCALIZANDO_ARQUIVO_LOCAL % caminho_csv_base)
            pyautogui.press('enter')
            self.driver.save_screenshot(fc.name_screenshot_default('Arquivo carregado com sucesso'))
            time.sleep(1)
            element_text = self.driver.find_element(By.CSS_SELECTOR, text_success)
            assert element_text.text == 'Arquivo carregado com sucesso'
            logger.logging.info(element_text.text)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def click_btn_save(self):
        """Clicar no botão para salvar a importação"""
        btn_save = 'div[class="form-group space-vertical-14"] > button[class="btn btn-success pull-right"'
        text_button = 'Salvar'
        click_btn_salvar = constants.CLICANDO_BOTAO % text_button
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, btn_save)
            self.driver.save_screenshot(fc.name_screenshot_default(click_btn_salvar))
            webdriver.ActionChains(self.driver).move_to_element(element).click().perform()
            logger.logging.info(click_btn_salvar)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % text_button)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def click_btn_cancel(self):
        """Clicar no botão para cancelar a importação"""
        btn_cancel = 'div[class="form-group space-vertical-14"] > button[class="btn btn-danger pull-right"'
        text_btn = 'Cancelar'
        click_btn = constants.CLICANDO_BOTAO % text_btn
        try:
            element = self.driver.find_element(By.CSS_SELECTOR, btn_cancel)
            self.driver.save_screenshot(fc.name_screenshot_default(click_btn))
            webdriver.ActionChains(self.driver).move_to_element(element).click().perform()
            logger.logging.info(click_btn)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def get_name_base(self):
        """Pegar o ID da primeira linha (a mais recente) da lista de tabelas de bases importadas."""
        try:
            row = self.get_table_html_bases_imported_json()[0]
            return row['Nome']
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def search_base_selected(self):
        """Realizando uma busca pela base selecionada no filtro"""
        try:
            nome_base_selecionada = self.get_name_base_random()
            element = self.driver.find_element(By.NAME, 'text')
            element.click()
            element.send_keys(nome_base_selecionada, Keys.ENTER)
            name_base_search = f'Busca pela base "{nome_base_selecionada}" sendo realizada.'
            logger.logging.info(name_base_search)
            self.driver.save_screenshot(fc.name_screenshot_default(name_base_search))
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def click_btn_details(self):
        """Clicar no botão detalhes de uma base ja importada. Sempre estará pegando a primeira da lista,
        ou seja a última a ser importada."""
        text_btn = 'Detalhes'
        click_btn = constants.CLICANDO_BOTAO % text_btn
        try:
            element = self.driver.find_element(By.XPATH, '//*[@id="items"]/table/tbody/tr[1]/td[8]/div/button')
            self.driver.save_screenshot(fc.name_screenshot_default(click_btn))
            element.click()
            logger.logging.info(click_btn)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % text_btn)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def click_btn_download(self, condition):
        """Clicar no botão Download e selecionar o diretório do arquivo a ser salvo."""
        link_text = 'Bases'
        btn_text = 'Download'
        click_btn = constants.CLICANDO_BOTAO % btn_text
        select_option = constants.SELECIONADO_OPCAO_DROPDOWN % (link_text, btn_text)
        try:
            self.driver.find_element(By.XPATH, '//*[@id="items"]/table/tbody/tr[1]/td[7]/div/button').click()
            logger.logging.info(click_btn)
            if condition == link_text:
                time.sleep(1)
                elemento = self.driver.find_element(By.XPATH, '//*[@id="items"]/table/tbody/tr[1]/td[7]/div/ul/li[1]/a')
                ActionChains(self.driver).move_to_element(elemento).click().perform()
                logger.logging.info(select_option)
                self.driver.save_screenshot(fc.name_screenshot_default(select_option))
                time.sleep(2)
            elif condition == 'Arquivo Inválido':
                pass
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % btn_text)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def get_name_base_random(self):
        """Pegando o nome de uma base aleatória das bases que foram listadas."""
        try:
            list_bases = self.get_table_html_bases_imported_json()
            base_selected = random.choice(list_bases)
            name_base_selected = base_selected["Nome"]
            logger.logging.info(f'A Base com o nome "{name_base_selected}" foi selecionada.')
            return name_base_selected
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def get_table_html_bases_imported_json(self):
        """Pegando a tabela do HTML com as 10 bases que estão listadas na tela,
        e transformando-as em um dicionário python, com chave:valor, chave=titulo_tabela, valor=valor_correspondente"""
        try:
            time.sleep(4)
            html = self.driver.page_source
            soup = BeautifulSoup(html, 'html.parser')
            html_titulo_tabela = soup.find('table', {'class': 'table my-table'}).find('thead').findAll('tr')
            html_tabela = soup.find('table', {'class': 'table my-table'}).find('tbody').findAll('tr')
            list_title = [base.getText().split('\n')[1:10] for base in html_titulo_tabela]
            list_bases = [base.getText().split('\n')[1:10] for base in html_tabela]
            count = 0
            list_json = []
            while count < len(list_bases[:-1]):
                dict_from_list = {k: v for k, v in zip(list_title[-1], list_bases[count])}
                list_json.append(dict_from_list)
                count += 1
            logger.logging.info(f'Bases encontradas: {list_json}')
            return list_json
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def get_text_data_imported(self):
        """Pegando o texto de dados importados na tela de Detalhes"""
        time.sleep(3)
        text_page = 'Detalhes da Base de dados'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(text_page))
            return self.driver.find_element(By.CSS_SELECTOR, 'div[class="col-xs-12 ng-scope"] > h1').text
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO % text_page)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def select_origin(self, origin):
        """Selecionado um filtro no dropdown Origem"""
        text_option = 'Origem'
        select_option = constants.SELECIONADO_OPCAO_DROPDOWN % (origin, text_option)
        try:
            element = self.driver.find_element(By.ID, 'origin')
            select = Select(element)
            select.select_by_visible_text(origin)
            self.driver.save_screenshot(fc.name_screenshot_default(select_option))
            logger.logging.info(select_option)
        except Exception as e:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % text_option)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise
