#language: pt

@all @gestor @bases @importar_base
Funcionalidade: Importar a base

  Contexto:
    Dado que esteja logado no produto "gestor" com o usuário "gestor-scob"

  Cenário: Importar uma base com sucesso com o usuário Gestor
    Quando preencher os dados para importar a base com "sucesso"
    E clicar no botão "Salvar" na janela de importar a base
    Então a base deve ser importada com sucesso

  Cenário: Importar uma base sem sucesso com o usuário Gestor
    Quando preencher os dados para importar a base com "sucesso"
    E clicar no botão "Cancelar" na janela de importar a base
    Então a base não deve ser importada

  Cenário: Fazer a busca por uma base especifica pelo nome
    Quando digitar o nome da base a ser buscada
    Então a base buscada deve ser a única listada

  Cenário: Clicar no botão "Detalhes" da lista de bases importadas
    Quando clicar no botão "Detalhes" de uma base importada
    Então a janela de detalhes deve ser exibida

  Esquema do Cenário: Realizar um filtro pela origem <origin>
    Quando clicar no checkBox "<origin>" da origem de uma base importada
    Então o filtro da base importada "<origin>" deve ser listada
    Exemplos:
      | origin     |
      | FTP        |
      | API        |
      | Plataforma |

  Cenário: Clicar no botão "Download" da lista de bases importadas
    Quando clicar no botão "Download" de uma base importada
    Então o download deverá ser realizado com sucesso