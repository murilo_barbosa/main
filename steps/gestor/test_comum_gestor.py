from behave import when, then

from features.steps.gestor.regras.functions.Regras import Regras
from features.utils import logger, constants, file_comum as fc

regras = Regras()


@when(u'clicar no menu "Regras"')
def clicar_menu_regras(context):
    global regras
    regras = Regras()
    regras.clicar_menu_regras()


@then(u'a janela inicial das "Regras" do gestor deverá ser aberta')
def validar_janela_regras(context):
    texto_esperado = 'Regras'
    try:
        texto_retornado = regras.validar_janela_inicial_regras()
        assert texto_esperado == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_esperado)
        regras.driver.save_screenshot(
            fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto_retornado))
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_esperado)
        raise
    finally:
        regras.driver.quit()
