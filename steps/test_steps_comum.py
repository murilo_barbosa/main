from behave import given
from features.steps.login.function.Login import Login
from features.utils import global_variables as gv, file_comum as fc


@given(u'que esteja logado no produto "{produto}" com o usuário "{usuario}"')
def login_por_produto(context, produto, usuario):
    match produto:
        case 'gestor':
            url = gv.url_gestor
        case 'gestor-regras':
            url = gv.url_gestor_regras
        case _:
            raise
    gv.url = url
    login = Login(user=usuario, url=gv.url)
    login.set_user()
    login.set_usermame()
    login.set_password()
    login.press_button_login()
    login.check_user_logged()
    gv.driver = login.driver
