import csv
import glob
import os
import re
import unidecode
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from features.support.Browser import Browser
from features.utils import logger, constants, global_variables as gv
from datetime import datetime, date

"""
- Conjunto de funções úteis compartilhadas no projeto. 
- Usar o padrão para aliás (apelido para o import) de fc.
"""


def get_driver():
    try:
        if gv.url is None:
            return gv.driver
        if gv.driver is None:
            gv.driver = Browser(gv.url).select_browser()
            return gv.driver
        else:
            return gv.driver
    except Exception as e:
        logger.logging.info(constants.ERRO_GENERICO % e)
        raise


def gerar_id_unico_com_datetime():
    """
    Gera uma sequecia numérica conforme a data/hora atual local, formando uma numeração única.
    """
    data = datetime.now()
    return str(data.timestamp()).replace('.', '')


def manipular_arquivo_csv(caminho_arquivo, tipo='r'):
    """
    Manipula os arquivos CSV. Por padrão está leitura.
    tipo='r': Ler arquivo csv
    tipo='w': Escreve em um arquivo
    tipo='a': Adiciona uma nova linha no CSV
    tipo='wa': Sobrescrever o arquivo com os novos dados
    """
    with open(caminho_arquivo, str(tipo)) as file:
        match tipo:
            case 'r':
                arquivo = csv.reader(file)
                linhas_arquivo = [linha for linha in arquivo]
                logger.logging.info(constants.ARQUIVO_SUCESSO % 'carregado')
                return linhas_arquivo
            case 'w' | 'a' | 'wa':
                arquivo = csv.writer(file)
                logger.logging.info(constants.ARQUIVO_SUCESSO % 'atualizado')
                return arquivo


def name_screenshot_default(test_name, path=gv.dir_pdf):
    """
    Define um padrão de nomenclatura e diretório dos screenshots com as evidências dos testes.
    """
    caminho_captura = None
    test_name = unidecode.unidecode(test_name)
    test_name = re.sub(r"[^a-zA-Z0-9.]", " ", test_name)
    try:
        diretorio_captura = f"{os.path.abspath(os.getcwd())}/features/support/arquivos/screenshots/{str(path)}"
        if not os.path.exists(diretorio_captura):
            os.makedirs(diretorio_captura)
        data = datetime.now().strftime('%Y-%m-%d_%H-%M-%S.%f')
        nome_arquivo_captura = f'{data}_{str(test_name)}.jpg'
        caminho_captura = f'{diretorio_captura}/{nome_arquivo_captura}'
        return caminho_captura
    except Exception as e:
        logger.logging.error(constants.ERRO_DIRETORIO_CAPTURA % caminho_captura)
        logger.logging.error(constants.ERRO_GENERICO % e)
        raise


def calcular_diferenca_data_dias(data_inicial, data_final):
    """Calcula a diferença em dias entre duas datas"""
    data_inicio = datetime.strptime(data_inicial, '%d/%m/%Y')
    data_fim = datetime.strptime(data_final, '%d/%m/%Y')
    return abs((data_fim - data_inicio).days)


def mm2p(milimetros):
    return milimetros / 0.352777


def gerar_pdf(title):
    title_pt = str(title).replace('<Scenario', 'Cenário: ').replace('>', "").replace('"', "").strip()
    name_file = title_pt.replace('Cenário: ', "").strip().replace(" ", "_")
    logger.logging.info(constants.GERANDO_PDF % title_pt)
    name = unidecode.unidecode(name_file).lower()
    name = re.sub(r"[^a-zA-Z0-9]", "_", name)
    date_today = date.today()
    imgs = 0
    cont = 195
    page = 1
    height_img = 85
    width_img = 200
    margin_page = 5
    num_steps = 1
    images = [os.path.join(gv.path_full_dir_pdf, nome) for nome in os.listdir(gv.path_full_dir_pdf)]
    try:
        pdf_local = f'features/support/arquivos/relatorios_pdf/{date_today}_{name}.pdf'
        pdf = canvas.Canvas(pdf_local, pagesize=A4)
        pdf.drawString(mm2p(margin_page), mm2p(290), title_pt.encode('utf-8'))
        for image in images:
            pdf.drawString(mm2p(margin_page), mm2p(cont + height_img + 1),
                           f'Passo {num_steps}: {date_today}_{os.path.basename(image)[27:-4]}')
            pdf.drawImage(image, mm2p(margin_page), mm2p(cont), width=mm2p(width_img), height=mm2p(height_img))
            cont -= 95
            imgs += 1
            num_steps += 1
            if imgs == 3:
                pdf.drawString(300, 2, f'Página {page}')
                pdf.showPage()
                cont = 195
                imgs = 0
                page += 1
        pdf.save()
        logger.logging.info(constants.ARQUIVO_GERADO_SUCESSO % ('PDF', pdf_local))
    except Exception as e:
        logger.logging.error(constants.ERRO_GERAR_ARQUIVO % 'PDF')
        logger.logging.error(e)
        raise


def remover_imagens_temporarias():
    py_files = glob.glob(f'features/support/arquivos/screenshots/{gv.dir_pdf}/*.jpg')
    try:
        if not os.path.exists(f'features/support/arquivos/screenshots/{gv.dir_pdf}'):
            return
        for py_file in py_files:
            os.remove(py_file)
        logger.logging.info('Removendo imagens temporárias')
    except OSError as e:
        logger.logging.error(f"Error:{e.strerror}")
        raise


def move_scroll_bar(driver):
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")


def procurar_valor_lista_regras(lista, chave, valor):
    """
    Procura o valor de um dicionário python, conforme os dados de entrada.
    Valores de saída: True (encontrou o valor dentro de um dicionário ou
    False quando não encontrou o valor procurado.
    """
    cont = 0
    while cont < len(lista):
        for v in lista:
            if v[chave] == valor:
                return True
            cont += 1
        return False


def remove_dict_dupicate_in_list(list_dict_python):
    """Função responsável por remover DicionáriosPython repetidos dentro de uma Lista.
    Quando houver mais de um dicionário dentro de uma lista repetido, aqui converte em uma tupla, remove a duplicidade,
    e devolve uma nova lista somente com os dicionáriosPython Distintos. Lembrando que para ser distinto, pode-se ter
    a mesma chave mas valor diferente. Irá remover somente se o Dicionário for IDÊNTICO."""
    seen = set()
    new_list = []
    try:
        for d in list_dict_python:
            t = tuple(d.items())
            if t not in seen:
                seen.add(t)
                new_list.append(d)
        return new_list
    except Exception as e:
        logger.logging.error(e)
        raise
