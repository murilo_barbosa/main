import os
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import IEDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.opera import OperaDriverManager
from features.utils import constants, logger, global_variables as gv


class Browser(object):
    def __init__(self, url, maximized=True):
        self.url = url
        self.path_default_downloads = os.path.abspath(os.getcwd() + '/features/support/arquivos/download')
        self.browser = gv.browser
        self.headless = gv.headless
        self.maximized = maximized
        self.driver = None

    def select_browser(self):
        try:
            logger.logging.info(constants.ABRINDO_NAVEGADOR)
            if not os.path.exists(self.path_default_downloads):
                os.mkdir(self.path_default_downloads)
            match self.browser:
                case 'chrome':
                    options = webdriver.ChromeOptions()
                    if self.headless:
                        options.add_argument("--headless")
                    prefs = {'download.default_directory': self.path_default_downloads}
                    options.add_experimental_option('prefs', prefs)
                    self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(),
                                                   chrome_options=options)
                case 'chromium':
                    self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
                case 'firefox':
                    from selenium.webdriver.firefox.options import Options
                    options = Options()
                    self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)
                case 'ie':
                    self.driver = webdriver.Ie(IEDriverManager().install())
                case 'edge':
                    self.driver = webdriver.Edge(EdgeChromiumDriverManager().install())
                case 'opera':
                    self.driver = webdriver.Opera(executable_path=OperaDriverManager().install())
                case _:
                    logger.logging.info(constants.NAVEGADOR_NAO_ENCONTRADO % self.browser)
                    return None
            logger.logging.info(constants.NAVEGADOR_ENCONTRADO % self.browser.upper())
            self.driver.get(self.url)
            logger.logging.info(constants.ACESSANDO_URL % self.url)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise
        finally:
            if self.maximized:
                self.driver.maximize_window()
            return self.driver
