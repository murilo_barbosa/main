from features.utils import logger, constants


class Credentials(object):
    user_gestor = "gestor-adm"
    user_gestor_scob = 'gestor-escob1@gestor.com'
    user_pontal = "pontal"
    password_default = "q1w2e3"

    def __init__(self):
        """Ao cadastrar um novo usuário, deve-se inserir o nome também na função get_user_name()"""
        self.user_name = None

    def get_user_name(self, user):
        match user:
            case self.user_gestor:
                self.user_name = 'Gestor-adm'
            case self.user_gestor_scob:
                self.user_name = 'gestor-escob1'
            case self.user_pontal:
                self.user_name = 'ADMIN'
            case _:
                logger.logging.error(constants.USUARIO_NAO_ENCONTRADO_CADASTRADO % user)
                raise
        return self.user_name
