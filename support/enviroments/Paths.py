from features.utils import global_variables as gv


class Paths(object):
    def __init__(self, url_base):
        gv.url = f'{url_base}/login'
        gv.url_gestor = f'{url_base}/bigboss/client/dashboard'
        gv.url_gestor_regras = f'{url_base}/bigboss/admin/rule'
