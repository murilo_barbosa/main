from features.support.enviroments.Paths import Paths
from features.utils import constants, logger


class SetEnvironments(object):
    def __init__(self, enviroment):
        match enviroment:
            case 'stg':
                self.url_base = 'http://staging-hub.pontaltech.com.br'
            case 'prd':
                # TODO Verificar qual a URL de PRD correta
                self.url_base = 'http://prd-hub.pontaltech.com.br'
            case _:
                logger.logging.info(constants.AMBIENTE_NAO_ENCONTRADO % enviroment)
        Paths(self.url_base)
